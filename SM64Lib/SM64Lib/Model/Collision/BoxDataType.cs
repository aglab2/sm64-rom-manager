﻿
namespace SM64Lib.Model.Collision
{
    public enum BoxDataType : short
    {
        Water = 0x44,
        ToxicHaze = 0x32,
        Mist = 0x33
    }
}