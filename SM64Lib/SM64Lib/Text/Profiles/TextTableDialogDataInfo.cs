﻿
namespace SM64Lib.Text.Profiles
{
    public class TextTableDialogDataInfo
    {
        public int TableRomOffset { get; set; }
    }
}