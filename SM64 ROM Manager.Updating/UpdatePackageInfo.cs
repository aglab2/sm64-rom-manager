﻿
namespace SM64_ROM_Manager.Updating
{
    public class UpdatePackageInfo
    {
        public string Name { get; set; }
        public ApplicationVersion Version { get; set; }
        public string Changelog { get; set; }
        public string Packagelink { get; set; }
    }
}